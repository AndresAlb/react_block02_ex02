import React from 'react'

const Names = (props) => {
    console.log(props)

    return (
	    	<h1>
	            {
	              props.name
	            }
	        </h1>
	        )

}
export default Names

Names.defaultProps = {
  names : ['Francesca','Luisa','Giuseppina','Pedro','Ana']
}
