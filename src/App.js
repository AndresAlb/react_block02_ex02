import React from 'react';
import Names from './Names.js';
import FirstNames from './FirstNames.js';
//import './App.css'


const App = () => {
  	const names = ['Francesca1','Luisa1','Name3','Name4','Name5']
  	const firstnames = ['fname1', 'fname2', 'fname3', 'fname4', 'fname5']

    return( 
            <div>
            {
            	names.map(function(item,index){
            		return ( <div>
            					<Names name = {item} /> 
            					<FirstNames firstnames = {firstnames[index]} />
            				</div>
            				)
            	})
            }

              
            </div>
                        
          )


}
export default App;



